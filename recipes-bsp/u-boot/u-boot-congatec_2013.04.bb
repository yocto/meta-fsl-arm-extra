# Congatec QMX6 u-boot

require recipes-bsp/u-boot/u-boot.inc

DESCRIPTION = "u-boot which includes support for Congatec Boards."
LICENSE = "GPLv2+"
LIC_FILES_CHKSUM = "file://COPYING;md5=1707d6db1d42237583f50183a5651ecb"


PROVIDES += "u-boot"

PV = "2013.04"

SRCREV = "b4569410751f6c51aaed3c84d554f1d01d8228e9"
SRCBRANCH = "cgt_imx_v2013.04_3.10.17_1.0.2"

SRC_URI = "git://git.congatec.com/arm/qmx6_uboot.git;protocol=http;branch=${SRCBRANCH} \
	   file://build-Fix-out-of-tree-build.patch \
"

S = "${WORKDIR}/git"

PACKAGE_ARCH = "${MACHINE_ARCH}"
COMPATIBLE_MACHINE = "(cgtqmx6)"
