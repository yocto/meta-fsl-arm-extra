#!/bin/sh
### BEGIN INIT INFO
# Provides:          gpios
# Required-Start:    $local_fs mountvirtfs
# Required-Stop:     $local_fs
# Default-Start:     S
# Default-Stop:      0 6
# Short-Description: export of Q7 GPIOs
### END INIT INFO

# script for exporting the GPIOs defined by the Q7 standard
#
# the GPIOs 0 ... 7 are currently mapped as inputs
# in order to define them as output, uncomment the "echo "out" ... direction" statement underneath the related pin export statement 
#
# Pin mapping (Q7 GPIO vs. GPIO nr)
#
# Q7, GPIO 0	<-->	gpio130
# Q7, GPIO 1	<-->	gpio86	(disabled)
# Q7, GPIO 2	<-->	gpio122
# Q7, GPIO 3	<-->	gpio123
# Q7, GPIO 4	<-->	gpio0
# Q7, GPIO 5	<-->	gpio111
# Q7, GPIO 6	<-->	gpio203
# Q7, GPIO 7	<-->	gpio110

do_start() {
	# Q7, Pin 185, GPIO 0
	echo 130 > /sys/class/gpio/export
	#echo "out" > /sys/class/gpio/gpio130/direction

	# Q7, Pin 186, GPIO 1
	# disabled, because this pin is shared with OTG_PWR functionality
	# in order to enable the pin as GPIO, the kernel's device tree configuration has to be changed
	#echo 86 > /sys/class/gpio/export
	#echo "out" > /sys/class/gpio/gpio86/direction

	# Q7, Pin 187, GPIO 2
	echo 122 > /sys/class/gpio/export
	#echo "out" > /sys/class/gpio/gpio122/direction

	# Q7, Pin 188, GPIO 3
	echo 123 > /sys/class/gpio/export
	#echo "out" > /sys/class/gpio/gpio123/direction

	# Q7, Pin 189, GPIO 4
	echo 0 > /sys/class/gpio/export
	#echo "out" > /sys/class/gpio/gpio0/direction

	# Q7, Pin 190, GPIO 5
	# Attention: could lead to a conflict when specified as output and used along with a backplane which has a LPC device assembled
	echo 111 > /sys/class/gpio/export
	#echo "out" > /sys/class/gpio/gpio111/direction

	# Q7, Pin 191 GPIO 6
	# Attention: could lead to a conflict when specified as output and used along with a backplane which has a LPC device assembled
	echo 203 > /sys/class/gpio/export
	#echo "out" > /sys/class/gpio/gpio203/direction

	# Q7, Pin 192, GPIO 7
	# Attention: could lead to a conflict when specified as output and used along with a backplane which has a LPC device assembled
	echo 110 > /sys/class/gpio/export
	#echo "out" > /sys/class/gpio/gpio110/direction

}

do_stop() {
	echo 110 > /sys/class/gpio/unexport
	echo 203 > /sys/class/gpio/unexport
	echo 111 > /sys/class/gpio/unexport
	echo 0 > /sys/class/gpio/unexport
	echo 123 > /sys/class/gpio/unexport
	echo 122 > /sys/class/gpio/unexport
	#echo 86 > /sys/class/gpio/unexport
	echo 130 > /sys/class/gpio/unexport
}

case "$1" in
	start)
	do_start
		;;
	restart|reload|force-reload)
		echo "Error: argument '$1' not supported" >&2
		exit 3
		;;
	stop)
	do_stop
		;;
	*)
		echo "Usage: $0 start|stop" >&2
		exit 3
		;;
esac

