# Congatec QMX6 u-boot

LICENSE = "GPLv2"
LIC_FILES_CHKSUM = "file://gpio.sh;md5=4585af1532d83f610c52a55f0955ce03"

SRC_URI_cgtqmx6 = " file://gpio.sh "
S = "${WORKDIR}"


do_configure() {
        :
}

do_compile() {
        :
}

do_install(){
	install -d ${D}${sysconfdir}/init.d
	install -m 0755 ${WORKDIR}/gpio.sh ${D}${sysconfdir}/init.d

        install -d ${D}${sysconfdir}/rc2.d
        install -d ${D}${sysconfdir}/rc3.d
        install -d ${D}${sysconfdir}/rc5.d

	update-rc.d -r ${D} gpio.sh start 99 2 3 5 .
}

FILES_${PN}_cgtqmx6 = "\
                 /etc/init.d/gpio.sh \
		 /etc/rc2.d/S99gpio.sh \
		/etc/rc5.d/S99gpio.sh \
		/etc/rc3.d/S99gpio.sh \
"


PACKAGE_ARCH_cgtqmx6 = "${MACHINE_ARCH}"
COMPATIBLE_MACHINE = "(cgtqmx6)"
