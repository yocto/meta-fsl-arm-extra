# Congatec QMX6 u-boot

LICENSE = "GPLv2+"
LIC_FILES_CHKSUM = "file://btevent/btevent.c;md5=6478ab41c6d7500d1b26ef8d1f032118"

SRCREV_cgtqmx6 = "8a0fdc959e43498692dbc3cc2c7d373ff2401778"
SRC_URI_cgtqmx6 = "git://git.congatec.com/arm/qmx6_addons.git;protocol=http;branch=master \
		   file://QMX6-Addons-Change-device-name.patch \
	   "
S = "${WORKDIR}/git"

do_compile(){
	cd btevent
	make
}

do_install() {
	install -d ${D}/usr/bin
	install -m 0777 ${S}/btevent/btevent ${D}/usr/bin
}

do_deploy () {
    install -d ${DEPLOYDIR}
    install -0777 ${S}/btevent/btevent ${DEPLOYDIR}/usr/bin
}

FILES_${PN} = "\
		/usr/bin/btevent \
"

PACKAGE_ARCH = "${MACHINE_ARCH}"
