SUMMARY = "Boomisc.sh with btevent daemon"

LICENSE = "GPLv2"
LIC_FILES_CHKSUM = "file://bootmisc.sh;md5=0fb58eb81ecdd438cabb26fedc3cfb3a"

RDEPENDS_${PN}_cgtqmx6 = "initscripts"

SRC_URI_cgtqmx6 = "file://bootmisc.sh"

S = "${WORKDIR}"

do_configure() {
        :
}

do_compile() {
        :
}

do_install(){
	install -d ${D}${sysconfdir}/init.d
	install -m 0755 ${WORKDIR}/bootmisc.sh ${D}${sysconfdir}/init.d
}

FILES_${PN}_cgtqmx6 = "\
                 /etc/init.d/bootmisc.sh \
"

PACKAGE_ARCH_cgtqmx6 = "${MACHINE_ARCH}"
